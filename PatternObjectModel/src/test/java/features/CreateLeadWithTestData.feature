Feature: Create Lead for Leaftaps 
#Background: 
#	Given open the browser 
#	And maximise the browser 
#	And load the url 
@smoke @reg 
Scenario Outline: Positive flow for Create Lead 
	And Enter the Username as DemoSalesManager 
	And Enter the password as crmsfa 
	And Click on the login button
	And Click on CRM/SFA link
	And CLick on Leads Tab
	And Click on Create Lead menu
	And Enter the Company Name as <cName> 
	And Enter the First name as <fName> 
	And Enter the Last name as <lName>
	When Click on the button CreateLead
	Then Verify Firstname of CreateLead <fName>
	Then Verify Lastname of CreateLead <lName>
	
	Examples: 
		|cName|fName|lName|
		|Opus|Renuka|Sudharson|
		|Opus|Shoba|P|
