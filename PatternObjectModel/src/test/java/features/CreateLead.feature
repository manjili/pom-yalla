Feature: Create Lead for Leaftaps
Scenario: Positive flow for Create Lead
Given open the browser
And maximise the browser
And load the url
And Enter the Username as DemoSalesManager
And Enter the password as crmsfa
And Click on the login button
And Click on CRM/SFA link
And CLick on Leads Tab
And Click on Create Lead menu
And Enter the Company Name as Opus
And Enter the First name as Renuka
And Enter the Last name as Sudharson
When Click on the button CreateLead
Then Verify CreateLead is Successful

