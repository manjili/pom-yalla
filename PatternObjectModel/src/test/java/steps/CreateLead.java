/*package steps;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead {
	ChromeDriver driver;
	
	@Given("open the browser")
	public void openTheBrowser() {
		System.setProperty("webdriver.chrome.driver",
				"./drivers/chromedriver.exe");
		 driver= new ChromeDriver();
		
	}

	@Given("maximise the browser")
	public void maximiseTheBrowser() {
		driver.manage().window().maximize();
	}

	@Given("load the url")
	public void loadTheUrl() {
		driver.get("http://leaftaps.com/opentaps/control/main");
	}

	@Given("Enter the Username as (.*)")
	public void enterTheUsernameAsDemoSalesManager(String Username) {
	  driver.findElementById("username").sendKeys(Username);
	  
	}

	@Given("Enter the password as (.*)")
	public void enterThePasswordAsCrmsfa(String pwd) {
		driver.findElementById("password").sendKeys(pwd);
	}

	@Given("Click on the login button")
	public void clickOnTheLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();
	}

	@Given("Click on CRM\\/SFA link")
	public void clickOnCRMSFALink() {
	   driver.findElementByLinkText("CRM/SFA").click();
	}

	@Given("CLick on Leads Tab")
	public void clickOnLeadsTab() {
		driver.findElementByLinkText("Leads").click();
	}

	@Given("Click on Create Lead menu")
	public void clickOnCreateLeadMenu() {
		driver.findElementByLinkText("Create Lead").click();
	}

	@Given("Enter the Company Name as (.*)")
	public void enterTheCompanyNameAsOpus(String cName) {
	   driver.findElementById("createLeadForm_companyName").sendKeys(cName);
	}

	@Given("Enter the First name as (.*)")
	public void enterTheFirstNameAsRenuka(String fName) {
		driver.findElementById("createLeadForm_firstName").sendKeys(fName);
	}

	@Given("Enter the Last name as (.*)")
	public void enterTheLastNameAsSudharson(String lName) {
		driver.findElementById("createLeadForm_lastName").sendKeys(lName);
	}

	@When("Click on the button CreateLead")
	public void clickOnTheButtonCreateLead() {
	    driver.findElementByClassName("smallSubmit").click();
	}

	@Then("Verify CreateLead is Successful")
	public void verifyCreateLeadIsSuccessful(String fName, String lName) {
		WebElement fname = driver.findElementByXPath("//span[@id='viewLead_firstName_sp']");
		WebElement lname = driver.findElementByXPath("//span[@id='viewLead_lastName_sp']");
		String ftext = fname.getText();
		System.out.println(ftext);
		String ltext = lname.getText();
		System.out.println(ltext);

		if (ftext.equalsIgnoreCase(fName)&& ltext.equalsIgnoreCase(lName)) {
			System.out.println("Lead created successfully");
		}
		
	  //System.out.println("Lead Created Successfully");
	}

}
*/