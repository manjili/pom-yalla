package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLeadPage extends Annotations{ 

	public CreateLeadPage() {
       PageFactory.initElements(driver, this);
	} 
	
	
	@FindBy(how=How.ID, using="createLeadForm_companyName") WebElement companyName;
	@And("Enter the Company Name as (.*)")
	public CreateLeadPage enterCompanyName(String cName) {
		clearAndType(companyName, cName);
		return this;	
		
	}
	
	@FindBy(how=How.ID, using="createLeadForm_firstName") WebElement firstName;
	@And("Enter the First name as (.*)")
	public CreateLeadPage enterFirstName(String fName) {
		clearAndType(firstName, fName);
		return this;	
		
	}
	
	@FindBy(how=How.ID, using="createLeadForm_lastName") WebElement lastName;
	@And("Enter the Last name as (.*)")
	public CreateLeadPage enterLastName(String lName) {
		clearAndType(lastName, lName);
		return this;	
		
	}

	@FindBy(how=How.CLASS_NAME, using="smallSubmit") WebElement buttoncreatelead;
	@When("Click on the button CreateLead")
	public ViewLeadPage clickCreateLeadButton() {
		click(buttoncreatelead);
		return new ViewLeadPage();	
		
	}
	
	@Then("Verify Firstname of CreateLead (.*)")
	public void verifyFirstname(String fName) {
		WebElement elefirstname = driver.findElementByXPath("//span[@id='viewLead_firstName_sp']");
		if((elefirstname.getText()).equals(fName)){
			System.out.println("Lead successfully with firstname"+elefirstname.getText());
		}
		
	}
	
	@Then("Verify Lastname of CreateLead (.*)")
	public void verifyCreateLeadIsSuccessful(String lName) {
		WebElement elelastname = driver.findElementByXPath("//span[@id='viewLead_lastName_sp']");
		if((elelastname.getText()).equals(lName)){
			System.out.println("Lead successfully with lastname"+elelastname.getText());
		}
	  
	}
}








