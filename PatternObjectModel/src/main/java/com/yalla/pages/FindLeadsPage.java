package com.yalla.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class FindLeadsPage extends Annotations{ 

	public FindLeadsPage() {
       PageFactory.initElements(driver, this);
	} 

	@FindBy(how=How.NAME, using="id") WebElement eleLeadId;
	public FindLeadsPage enterLeadId(String leadid) {
		clearAndType(eleLeadId, leadid);
		return this;
		
		
	}
	
	@FindBy(how=How.XPATH, using="//button[text()='Find Leads']") WebElement findleadbutton;
	public FindLeadsPage clickFindLeadsbutton() {
		click(findleadbutton);
		return this;
				
	}
	
	/*@FindBy(how=How.XPATH, using="//div[@class='x-grid3-scroller']") WebElement table;
	public ViewLeadPage clickLeadId(String leadid) {
		List<WebElement> allrows = table.findElements(By.tagName("tr"));
		//locateElements(type, value);
		for (WebElement eachrow : allrows) {
			String text = eachrow.getText();
			System.out.println(text);
			if(text.contentEquals(leadid)) {
				WebElement id = locateElement("id", leadid);
				click(id);
			}
				
		}
		return new ViewLeadPage();
						
	}*/
	
	@FindBy(how=How.XPATH, using="//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a") WebElement eleLeadID;
	public ViewLeadPage clickLeadId(String leadid) {
		String elementText = getElementText(eleLeadID);
		System.out.println(elementText);
		if(elementText.contentEquals(leadid))
			click(eleLeadID);
		return new ViewLeadPage();
	}
	
	
	
	@FindBy(how=How.XPATH, using="//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a") WebElement leadIdToMerge;
	public MergeLeadPage clickLeadIdToMerge(String mergeleadid) {
		String elementText = getElementText(leadIdToMerge);
		System.out.println("The text of Lead to be Merged is:"+elementText);
		if(elementText.contentEquals(mergeleadid))
			clickWithNoSnap(leadIdToMerge);
			switchToWindow(0);
			return new MergeLeadPage();
	}
	
	@FindBy(how=How.CLASS_NAME, using="x-paging-info") WebElement norecordsmsg;
	public FindLeadsPage verifynorecordsmsg() {
		String message = getElementText(norecordsmsg);
		String expectedmsg = "No records to display";
		if (message.equalsIgnoreCase(expectedmsg)) {
			System.out.println("Task completed.No receords to display");}
		
		return this;
				
	}
	
}







