package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class ViewLeadPage extends Annotations{ 

	public ViewLeadPage() {
       PageFactory.initElements(driver, this);
	} 

	@FindBy(how=How.ID, using="viewLead_firstName_sp") WebElement viewfirstname;
	public ViewLeadPage verifyFirstName(String fName) {
		verifyExactText(viewfirstname, fName);
		return this;
			
	}
	
	
	@FindBy(how=How.ID, using="viewLead_lastName_sp") WebElement viewlastname;
	public ViewLeadPage verifyLastName(String lName) {
		verifyExactText(viewlastname, lName);
		return this;
			
	}
	
	@FindBy(how=How.ID, using="viewLead_companyName_sp") WebElement viewcompanyname;
	public ViewLeadPage verifyDuplicateID(String leadid) {
		String elementText = getElementText(viewcompanyname);
		if(elementText!="Opus ("+leadid+")")
		System.out.println("Lead duplicated successfully");
		return this;
			
	}
	
	@FindBy(how=How.LINK_TEXT, using="Edit") WebElement editbutton;
	public OpentapsEditPage clickEdit() {
		click(editbutton);
		return new OpentapsEditPage();
			
	}
	
	@FindBy(how=How.XPATH, using="//span[@id='viewLead_industryEnumId_sp']") WebElement eleindustry;
	public ViewLeadPage verifyIndustry(String industry) {
		String elementText = getElementText(eleindustry);
		if (elementText.equalsIgnoreCase(industry)) {
			System.out.println("Lead edited successfully");
		}
		return this;
			
	}
	
	@FindBy(how=How.LINK_TEXT, using="Find Leads") WebElement findleads;
	public FindLeadsPage clickfindleads() {
		click(findleads);
		return new FindLeadsPage();
			
	}
	
	@FindBy(how=How.LINK_TEXT, using="Delete") WebElement deletelead;
	public MyLeadsPage clickDeleteLead() {
		click(deletelead);
		return new MyLeadsPage();
			
	}
	
	@FindBy(how=How.LINK_TEXT, using="Duplicate Lead") WebElement duplicatelead;
	public DuplicateLeadPage clickDuplicateLead() {
		click(duplicatelead);
		return new DuplicateLeadPage();
			
	}
	
}







