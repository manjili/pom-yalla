package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;

public class MyLeadsPage extends Annotations{ 

	public MyLeadsPage() {
       PageFactory.initElements(driver, this);
	} 

	@FindBy(how=How.LINK_TEXT, using="Create Lead") WebElement elecreatelead;
	@And("Click on Create Lead menu")
	public CreateLeadPage clickCreateLead() {
		click(elecreatelead);
		return new CreateLeadPage();
		
		
	}
	
	@FindBy(how=How.LINK_TEXT, using="Find Leads") WebElement elefindlead;
	public FindLeadsPage clickFindLead() {
		click(elefindlead);
		return new FindLeadsPage();
		
		
	}
	
	@FindBy(how=How.LINK_TEXT, using="Merge Leads") WebElement elemergelead;
	public MergeLeadPage clickMergeLead() {
		click(elemergelead);
		return new MergeLeadPage();
		
		
	}
	

}







