package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class MergeLeadPage extends Annotations{ 

	public MergeLeadPage() {
       PageFactory.initElements(driver, this);
	} 

	@FindBy(how=How.XPATH, using="//img[@src='/images/fieldlookup.gif']") WebElement eleFromLead ;
	public FindLeadsPage clickFromLead() {
		clickWithNoSnap(eleFromLead);
		switchToWindow(1);
		return new FindLeadsPage();
			
	}
	
	@FindBy(how=How.XPATH, using="(//img[@src='/images/fieldlookup.gif'])[2]") WebElement eleToLead ;
	public FindLeadsPage clickToLead() {
		clickWithNoSnap(eleToLead);
		switchToWindow(1);
		return new FindLeadsPage();
			
	}
	
	@FindBy(how=How.LINK_TEXT, using="Merge") WebElement eleMerge ;
	public ViewLeadPage clickMerge() {
		clickWithNoSnap(eleMerge);
		switchToAlert();
		getAlertText();
		acceptAlert();
		return new ViewLeadPage();
			
	}
	
	
	

	
}







