package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class OpentapsEditPage extends Annotations{ 

	public OpentapsEditPage() {
       PageFactory.initElements(driver, this);
	} 

	@FindBy(how=How.XPATH, using="//select[@name='industryEnumId']") WebElement eleindustry;
	public OpentapsEditPage Chooseindustry(String industry) {
		selectDropDownUsingText(eleindustry, industry);
		return this;
		
	}
	
	@FindBy(how=How.XPATH, using="//input[@value='Update']") WebElement eleupdate;
	public ViewLeadPage clickUpdate() {
		click(eleupdate);
		return new ViewLeadPage();
	}
	

}







