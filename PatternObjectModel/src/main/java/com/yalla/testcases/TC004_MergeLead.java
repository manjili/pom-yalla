package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC004_MergeLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC004_Merge";
		testcaseDec = "Merging two Leads in leaftaps";
		author = "Manjili";
		category = "smoke";
		excelFileName = "TC004";
	} 

	@Test(dataProvider="fetchData")
	public void mergelead(String uName, String pwd, String fromleadID, String toleadID ) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCrmsfa()
		.clickLeads()
		.clickMergeLead()
		.clickFromLead()
		.enterLeadId(fromleadID)
		.clickFindLeadsbutton()
		.clickLeadIdToMerge(fromleadID)
		.clickToLead()
		.enterLeadId(toleadID)
		.clickFindLeadsbutton()
		.clickLeadIdToMerge(toleadID)
		.clickMerge()
		.clickfindleads()
		.enterLeadId(fromleadID)
		.clickFindLeadsbutton()
		.verifynorecordsmsg();
		
		
		
		
		
		
		
		
	
		
	}
	
}






