package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC006_DuplicateLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC006_DuplicateLead";
		testcaseDec = "Duplicating a Lead in leaftaps";
		author = "Manjili";
		category = "smoke";
		excelFileName = "TC006";
	} 

	@Test(dataProvider="fetchData")
	public void duplicateLead(String uName, String pwd, String leadid ) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCrmsfa()
		.clickLeads()
		.clickFindLead()
		.enterLeadId(leadid)
		.clickFindLeadsbutton()
		.clickLeadId(leadid)
		.clickDuplicateLead()
		.clickCreateLeadButton()
		.verifyDuplicateID(leadid);
		

		
		
		
		
		
		
		
	
		
	}
	
}






