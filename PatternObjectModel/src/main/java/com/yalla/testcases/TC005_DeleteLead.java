package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC005_DeleteLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC005_Delete";
		testcaseDec = "Deleting a Lead in leaftaps";
		author = "Manjili";
		category = "smoke";
		excelFileName = "TC005";
	} 

	@Test(dataProvider="fetchData")
	public void deleteLead(String uName, String pwd, String leadid ) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCrmsfa()
		.clickLeads()
		.clickFindLead()
		.enterLeadId(leadid)
		.clickFindLeadsbutton()
		.clickLeadId(leadid)
		.clickDeleteLead()
		.clickFindLead()
		.enterLeadId(leadid)
		.clickFindLeadsbutton()
		.verifynorecordsmsg();
	
		

		
		
		
		
		
		
		
	
		
	}
	
}






