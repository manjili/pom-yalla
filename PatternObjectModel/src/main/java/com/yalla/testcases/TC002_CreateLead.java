package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC002_CreateLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_CreateLead";
		testcaseDec = "Creating a Lead in leaftaps";
		author = "Manjili";
		category = "smoke";
		excelFileName = "TC002";
	} 

	@Test(dataProvider="fetchData")
	public void createLead(String uName, String pwd, String cName, String fName, String lName) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCrmsfa()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName(cName)
		.enterFirstName(fName)
		.enterLastName(lName)
		.clickCreateLeadButton()
		.verifyFirstName(fName)
		.verifyLastName(lName);

		
	}
	
}






