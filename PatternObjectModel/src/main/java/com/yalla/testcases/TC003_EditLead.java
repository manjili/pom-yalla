package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC003_EditLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC003_EditLead";
		testcaseDec = "Editing a Lead in leaftaps";
		author = "Manjili";
		category = "smoke";
		excelFileName = "TC003";
	} 

	@Test(dataProvider="fetchData")
	public void editLead(String uName, String pwd, String leadID, String industry ) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCrmsfa()
		.clickLeads()
		.clickFindLead()
		.enterLeadId(leadID)
		.clickFindLeadsbutton()
		.clickLeadId(leadID)
		.clickEdit()
		.Chooseindustry(industry)
		.clickUpdate()
		.verifyIndustry(industry);
		
	}
	
}






